import os
from setuptools import setup


# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name='urllib2utils',
    version='0.1',
    description='Utilities to deal with conditional GET built on top of urllib2',
    author='Michael Murtaugh',
    author_email='mm@automatist.net',
    url='http://git.constantvzw.org/aa.urllib2utils.git',
    py_modules=['urllib2utils'],
    license = "GPL3",
    keywords = "http",
    long_description=read('README'),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Programming Language :: Python",
        "Topic :: Utilities",
    ],
)
